﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stoktakip
{
    public class kasatakipbilgileri
    {
        public int ID { get; set; }
        public string KasayaGirenTutar { get; set; }
        public string KasadanCıkanTutar { get; set; }
        public string Tarih { get; set; }
        public string SatilanÜrünAdi { get; set; }
        public int SatilanÜrünSayisi { get; set; }
        public int SatilanÜrünFiyati { get; set; }
    }
}