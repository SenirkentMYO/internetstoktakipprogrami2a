﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Toptancılar.aspx.cs" Inherits="Stoktakip.Toptancılar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Panel ID="Panel1" runat="server" BackColor="#999966" Height="659px">
            <asp:Image ID="Image1" runat="server" Height="167px" ImageUrl="~/Resimler/banner.png" Width="1259px" />
            <br />
            <br />
            <br />
            <br />
            <br />
            <asp:Label ID="Label2" runat="server" Text="FİRMA ADI"></asp:Label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
            <asp:TextBox ID="TextBox1" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label3" runat="server" Text="ALINAN URUN SAYISI"></asp:Label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox2" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label1" runat="server" Text="ÖDENECEK TUTAR"></asp:Label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
            <asp:TextBox ID="TextBox3" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label5" runat="server" Text="ÖDENEN TUTAR"></asp:Label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
            <asp:TextBox ID="TextBox4" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label6" runat="server" Text="KALAN BORÇ"></asp:Label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
            <asp:TextBox ID="TextBox5" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label4" runat="server" Text="FİRMA TEL NO"></asp:Label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox6" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label7" runat="server" Text="GELİŞ FİYATI"></asp:Label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox7" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
            <br />
            &nbsp;<br />
            <asp:Button ID="Button1" runat="server" Text="SİPARİŞ VER" OnClick="Button1_Click" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button2" runat="server" Text="DATALİST LİSTELE" OnClick="Button2_Click" Width="129px" />
            <br />
            <asp:DataList ID="DataList1" runat="server" RepeatColumns="7" OnSelectedIndexChanged="DataList1_SelectedIndexChanged">
                <ItemTemplate>
                    FİRMA ADI=<%# Eval("FirmaAdı") %><br />FİRMA TEL=<%# Eval("ToptancıTelNo") %><br />ALINAN MİKTAR=<%# Eval("AlınanUrunMiktar") %><br />GELİŞ FİYATI=<%# Eval("UrununGelisFiyatı") %><br />ÖDENECEK TUTAR=<%# Eval("ToptancıyaOdenecekTutar") %><br />ÖDENEN TUTAR=<%# Eval("ToptancıyaOdenenTutar") %><br />KALAN BORÇ=<%# Eval("ToptaancıyaOlanBorc") %><br /><hr /><asp:Button ID="dzenlebtn" runat="server" Text="Düzenle" /> <asp:Button ID="silbtn" runat="server" Text="Sil" />
                </ItemTemplate>
            </asp:DataList>
            <br />
            <br />
        </asp:Panel>
    
    </div>
    </form>
</body>
</html>
