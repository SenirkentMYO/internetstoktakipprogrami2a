﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stoktakip
{
    public class Toptancı
    {
        public int ID { get; set; }
        public string FirmaAdı { get; set; }
        public string ToptancıTelNo { get; set; }
        public string AlınanUrunMiktar { get; set; }
        public string UrununGelisFiyatı { get; set; }
        public string ToptancıyaOdenecekTutar { get; set; }
        public string ToptancıyaOdenenTutar { get; set; }
        public string ToptaancıyaOlanBorc { get; set; }
    }
}