﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Stoktakip
{
    public partial class ÜrünStoklar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            List<Stokbilgilerii> StokListesi = new List<Stokbilgilerii>();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["baglan"].ToString());
            con.Open();
            {
                SqlCommand comand = new SqlCommand("Select * From urunstokbilgileri", con);
                SqlDataReader satirlar = comand.ExecuteReader();
                while (satirlar.Read())
                {
                    Stokbilgilerii sb = new Stokbilgilerii();
                    sb.UrunAdı = TextBox1.Text;
                    sb.UrunKodu = TextBox2.Text;
                    sb.DepodakiToplamUrunSayısı = Convert.ToInt32(TextBox3.Text);
                    sb.SatılanUrunSayısı = Convert.ToInt32(TextBox4.Text);
                    sb.UrunFiyat = Convert.ToInt32(TextBox5.Text);
                    sb.KDVOranı = Convert.ToInt32(TextBox6.Text);

                    StokListesi.Add(sb);
                    con.Close();
                    DataList1.DataSource = StokListesi;
                    DataList1.DataBind();
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}