﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Stoktakip
{
    public partial class KasaTakip : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            List<kasatakipbilgileri> SatısListesi = new List<kasatakipbilgileri>();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["baglan"].ToString());
            con.Open();
            {
                SqlCommand comand = new SqlCommand("Select * From kasatakipbilgileri", con);
                SqlDataReader satirlar = comand.ExecuteReader();
                while (satirlar.Read())
                {
                    kasatakipbilgileri ktb = new kasatakipbilgileri();
                    ktb.SatilanÜrünAdi = TextBox1.Text;
                    ktb.SatilanÜrünSayisi =Convert.ToInt32(TextBox2.Text);
                    ktb.SatilanÜrünFiyati = Convert.ToInt32(TextBox3.Text);
                    ktb.KasayaGirenTutar = TextBox4.Text;
                    ktb.KasadanCıkanTutar = TextBox5.Text;
                    ktb.Tarih = TextBox6.Text;

                    SatısListesi.Add(ktb);
                    con.Close();
                    Repeater1.DataSource = SatısListesi;
                    Repeater1.DataBind();
                }
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

        }
    }
}