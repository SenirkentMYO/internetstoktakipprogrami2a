﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Stoktakip
{
    public partial class Toptancılar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            List<Toptancı> Toptancılr = new List<Toptancı>();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["baglan"].ToString());
            con.Open();
            {
                SqlCommand comand = new SqlCommand("Select * From toptancıfirmabilgileri", con);
                SqlDataReader satirlar = comand.ExecuteReader();
                while (satirlar.Read())
                {
                    Toptancı tp = new Toptancı();
                    tp.FirmaAdı = TextBox1.Text;
                    tp.AlınanUrunMiktar = TextBox2.Text;
                    tp.ToptancıyaOdenecekTutar = TextBox3.Text;
                    tp.ToptancıyaOdenenTutar = TextBox4.Text;
                    tp.ToptaancıyaOlanBorc = TextBox5.Text;
                    tp.ToptancıTelNo = TextBox6.Text;
                    tp.UrununGelisFiyatı = TextBox7.Text;
                    Toptancılr.Add(tp);
                    con.Close();
                    DataList1.DataSource = Toptancılr;
                    DataList1.DataBind();
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }

        protected void DataList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}