﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stoktakip
{
    public class Stokbilgilerii
    {
        public string UrunAdı {get;set;}
        public string UrunKodu{get;set;}
        public int DepodakiToplamUrunSayısı {get;set;}
        public int SatılanUrunSayısı    {get;set;}
        public int UrunFiyat    {get;set;}
        public int KDVOranı   {get;set;}
    }
    }
